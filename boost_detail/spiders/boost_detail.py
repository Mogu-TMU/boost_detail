import re
import csv
import scrapy
import datetime
import codecs

class Boost_detailSpider(scrapy.Spider):
	name                   = 'boost_detail'
	allowed_domains        = ['ssp.isboost.co.jp']
	handle_httpstatus_list = [303, 302, 301, 200]
	custom_settings        = {
		'DUPEFILTER_CLASS': 'scrapy.dupefilters.BaseDupeFilter',
	}
	cookie_var             = {}

	def start_requests(self):
		url                = 'https://ssp.isboost.co.jp/app/login'

		yield scrapy.Request(url=url, callback=self.parse_boost)

	def parse_boost(self, response):
		self.get_cookies(response.headers.getlist('Set-Cookie'))
		url                = 'https://ssp.isboost.co.jp/heimdallr/login'
		formdata           = {
			'domain': 'ssp.isboost.co.jp',
			'client_id': 'aladdin',
			'redirect_uri': 'https://ssp.isboost.co.jp/app/login/check-custom',
			'response_type': 'code',
			'service': 'aladdin',
			'username': 'boost-ins04yS7afPcD',
			'password': 'mX8MyVFEe5B2ZgaT',
			'_submit': 'Log in',
		}

		yield scrapy.FormRequest(url=url, formdata=formdata, method='POST', cookies=self.cookie_var, callback=self.parse_login)

	def parse_login(self, response):
		url                = response.headers['Location'].decode('utf-8')
		self.get_cookies(response.headers.getlist('Set-Cookie'))
		meta               = {
			'dont_redirect': True,
		}

		yield scrapy.Request(url=url, cookies=self.cookie_var, meta=meta, dont_filter=True, callback=self.parse_check)
		
	def parse_check(self, response):
		self.get_cookies(response.headers.getlist('Set-Cookie'))
		url                = 'https://ssp.isboost.co.jp/app/'
		meta               = {
			'dont_redirect': True,
		}

		yield scrapy.Request(url=url, cookies=self.cookie_var, meta=meta, dont_filter=True, callback=self.parse_app)

	def parse_app(self, response):
		url                = 'https://ssp.isboost.co.jp/app/publisher/'
		meta               = {
			'dont_redirect': True,
		}
		yield scrapy.Request(url=url, cookies=self.cookie_var, meta=meta, dont_filter=True, callback=self.parse_publisher)

	def parse_publisher(self, response):
		meta               = {
			'dont_redirect': True,
		}
		tr = response.css('#g1_content-main table.records_list tbody tr')
		for td in tr:
			half_url       = td.css('a.btn-primary').attrib['href']
			url            = 'https://ssp.isboost.co.jp' + str(half_url)
			yield scrapy.Request(url=url, cookies=self.cookie_var, meta=meta, dont_filter=True, callback=self.parse_form)
			break
		
		nav = response.css('#g1_content-main div.navigation li > a[rel="next"]').attrib['href']
		if len(nav) > 0:
			next_url = 'https://ssp.isboost.co.jp' + nav
			yield scrapy.Request(url=next_url, cookies=self.cookie_var, meta=meta, dont_filter=True, callback=self.parse_publisher)

	def parse_form(self, response):
		cols               = response.css('#vendor_edit_publisher_form div.form-group')
		for col in cols:
			if col.css('label::text').get() == 'メールアドレス':
				email      = col.css('input').attrib['value']
				if len(email) < 0:
					email  = 'none'
				print(email)

	def get_cookies(self, t_cookie):
		if len(t_cookie) > 0:
			cookie_list = []
			for val in t_cookie:
				value        = val.decode('utf-8')
				cookie_list.extend(list(i for i in value.split(';')))
			for var in cookie_list:
				if 'genieeadserver' in var:
					self.cookie_var['genieeadserver'] = var.split('genieeadserver=')[1]
				elif 'authenticator' in var:
					self.cookie_var['authenticator'] = var.split('authenticator=')[1]